var path = require('path');
var webpack = require('webpack');

var bourbon = require('bourbon');


module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'build.js',
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules'),
  },
  sassLoader: {
    includePaths: Array.prototype.concat(
      [path.join(__dirname, 'node_modules', 'normalize.css')],
      bourbon.includePaths
    )
  },
  vue: {
    loaders: {
      scss: 'vue-style!css!sass',
    },
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue',
      },
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/,
      },
      {
        test: /\.(sass|scss)$/,
        loader: 'style!css!sass',
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file',
        query: {
          name: '[name].[ext]?[hash]',
        },
      },
      {
        test: /\.(webm|mp3)$/,
        loader: 'url',
        query: {
          name: '[name].[ext]?[hash]',
          limit: 10000,
        },
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
  },
  devtool: '#eval-source-map',
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.EnvironmentPlugin([
      'NODE_ENV',
    ]),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
    }),
  ]);
}
